<?php

error_reporting(E_ALL);
include __DIR__.'/controller/UserController.php';
$UserController = new UserController;

if(isset($_GET['action']) && $_GET['action'] == 'delete')
{
	$UserController->delete($_GET['id']);
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>User Maintain</title>
</head>
<body>
	<a href="add.php?type=user">
		<button>Add User</button>
	</a>
	<?php if(isset($_GET['status']) && $_GET['status'] == 'success'):?>
		<?php echo $_GET['message'];?>
	<?php endif;?>
	<?php if(isset($_GET['status']) && $_GET['status'] == 'error'):?>
		<?php echo $_GET['message'];?>
	<?php endif;?>
	<table border="1">
		<thead>
			<tr>
				<td>Id</td>
				<td>Full Name</td>
				<td>Username</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($UserController->list() as $key => $value):?>
			<tr>
				<td><?php echo $value['id'];?></td>
				<td><?php echo $value['full_name'];?></td>
				<td><?php echo $value['username'];?></td>
				<td>
					<a href="edit.php?type=user&id=<?php echo $value['id'];?>">Edit</a>
					<a href="?action=delete&id=<?php echo $value['id'];?>">Delete</a>
				</td>
			</tr>
			<?php endforeach;?>
			<?php if(sizeof($UserController->list()) == 0):?>
			<tr>
				<td colspan="4">No users found!</td>
			</tr>
			<?php endif;?>
		</tbody>
	</table>
</body>
</html>