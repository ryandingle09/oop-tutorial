<?php

class Core
{
	private $host 	= 'localhost';
	private $user 	= 'root';
	private $pass 	= '';
	private $db 	= 'oop';

	protected $connection;

	public function __construct()
	{
		if(!isset($this->connection))
		{
			$this->connection = new mysqli($this->host, $this->user, $this->pass, $this->db);

			if(!$this->connection) 
			{
				echo 'Database connection error!';
				exit;
			}
		}	

		return $this->connection;
	}
}

?>