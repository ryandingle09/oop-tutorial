<?php

include __DIR__.'/../core/Core.php';

class UserController extends Core
{	
	function __construct()
	{
		parent::__construct();
		//check for session here if needed
	}

	/*showing the list of users*/
	public function list()
	{
		$query 	= $this->connection->query("SELECT * FROM users");
		$rows 	= [];

		while($row = $query->fetch_assoc())
			$rows[] = $row;

		return $rows;
	}

	/*insert user function*/
	public function add($fullname, $username, $password, $confirm_password)
	{
		$validator 	= $this->validate($fullname, $username, $password, $confirm_password);
		if($validator) return $validator;

		$username = $this->check_username($username);
		if($username) return $username;

		$sql 		= "INSERT INTO users (username, password, full_name)values('$username', '".sha1($password)."', '$fullname')";
		$query 		= $this->connection->query($sql);

		if($query) header('location: index.php?status=success&message=User Successfully Added!');
		else return ['error' => 'Adding user error.'];
	}

	/*udpate user function*/
	public function update($id, $fullname, $username, $password, $confirm_password)
	{
		$validator 	= $this->validate($fullname, $username, $password, $confirm_password);
		if($validator) return $validator;

		if($this->get($id)[0]['username'] != $username)
		{
			$username = $this->check_username($username);
			if($username) return $username;
		}

		$sql 		= "UPDATE users SET username = '$username', password = '".sha1($password)."', full_name = '$fullname' WHERE id='$id'";
		$query 		= $this->connection->query($sql);

		if($query) header('location: index.php?status=success&message=User Successfully Updated!');
		else return ['error' => 'Update user error.'];
	}

	/*get user data function*/
	public function get($id)
	{
		$query 	= $this->connection->query("SELECT * FROM users WHERE id = ".$id." LIMIT 1");
		$rows 	= [];

		while($row = $query->fetch_assoc())
			$rows[] = $row;

		return $rows;
	}

	/*delete data function*/
	public function delete($id)
	{
		$query 	= $this->connection->query("DELETE FROM users WHERE id = ".$id."");
		if($query) header('location: index.php?status=success&message=Successfully Deleted.');
		else return ['Unable to delete data.'];
	}

	/*user input validation*/
	public function validate($fullname, $username, $password, $confirm_password)
	{
		$validations = [];

		if(empty($username)) $validations[] 		= 'Username field is required.';
		if(empty($fullname)) $validations[] 		= 'Fullname field is required.';
		if(empty($password)) $validations[] 		= 'Password field is required.';
		if(empty($confirm_password)) $validations[] = 'Confirm password field is required.';

		if(!empty($confirm_password) && !empty($password))
			if($password !== $confirm_password) $validations[] = 'Password do not match.';

		if(sizeof($validations) == 0) return false;
		else return $validations;
	}

	/*check username validatation*/
	public function check_username($username)
	{
		$check 	= $this->connection->query("SELECT count(*) as count FROM users where username = '$username'");
		$rows 	= [];

		while ($row = $check->fetch_assoc()) 
			return ($row['count'] != 0) ? ['Username already exists.'] : false;
	}
}


?>