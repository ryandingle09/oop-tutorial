<?php

error_reporting(E_ALL);
include __DIR__.'/controller/UserController.php';
$UserController = new UserController;

if(isset($_POST['save']))
{
	$fullname = $_POST['fullname'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$confirm_password = $_POST['confirm_password'];

	$validations = $UserController->update($_GET['id'], $fullname, $username, $password, $confirm_password);
}

$data = $UserController->get($_GET['id']);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit User</title>
</head>
<body>
	<a href="index.php">Back</a>
	<h1>Edit User</h1>
	<form method="POST">
		<?php if (isset($validations)):?>
		<?php foreach ($validations as $value):?>
			<?php echo $value.'<br>';?>
		<?php endforeach;?>
		<?php endif;?>
		<?php if (isset($validations)):?>
			<?php echo isset($validations[0]['success']) ? $validations[0]['success'].'<br>' : '';?>
			<?php echo isset($validations[0]['error']) ? $validations[0]['error'].'<br>' : '';?>
		<?php endif;?>
		<table border="1">
			<tbody>
				<tr>
					<td>Full Name</td>
					<td><input type="text" name="fullname" value="<?php echo (isset($_POST['fullname'])) ? $_POST['fullname'] : $data[0]['full_name'];?>"></td>
				</tr>
				<tr>
					<td>Username</td>
					<td><input type="text" name="username" value="<?php echo (isset($_POST['username'])) ? $_POST['username'] : $data[0]['username'];?>"></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="password" value="<?php echo (isset($_POST['password'])) ? $_POST['password'] : '';?>"></td>
				</tr>
				<tr>
					<td>Confirm Password</td>
					<td><input type="password" name="confirm_password" value="<?php echo (isset($_POST['confirm_password'])) ? $_POST['confirm_password'] : '' ;?>"></td>
				</tr>
				<tr>
					<td colspan="2"><button name="save" type="submit">Save</button></td>
				</tr>
			</tbody>
		</table>
	</form>
</body>
</html>